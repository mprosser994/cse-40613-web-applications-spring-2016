
// instrument by jscoverage, do not modifly this file
(function (file, lines, conds, source) {
  var BASE;
  if (typeof global === 'object') {
    BASE = global;
  } else if (typeof window === 'object') {
    BASE = window;
  } else {
    throw new Error('[jscoverage] unknow ENV!');
  }
  if (BASE._$jscoverage) {
    BASE._$jscmd(file, 'init', lines, conds, source);
    return;
  }
  var cov = {};
  /**
   * jsc(file, 'init', lines, condtions)
   * jsc(file, 'line', lineNum)
   * jsc(file, 'cond', lineNum, expr, start, offset)
   */
  function jscmd(file, type, line, express, start, offset) {
    var storage;
    switch (type) {
      case 'init':
        if(cov[file]){
          storage = cov[file];
        } else {
          storage = [];
          for (var i = 0; i < line.length; i ++) {
            storage[line[i]] = 0;
          }
          var condition = express;
          var source = start;
          storage.condition = condition;
          storage.source = source;
        }
        cov[file] = storage;
        break;
      case 'line':
        storage = cov[file];
        storage[line] ++;
        break;
      case 'cond':
        storage = cov[file];
        storage.condition[line] ++;
        return express;
    }
  }

  BASE._$jscoverage = cov;
  BASE._$jscmd = jscmd;
  jscmd(file, 'init', lines, conds, source);
})('lib/markitondemand.js', [1,5,7,25,33,41,62,63,64,65,8,9,10,13,16,20,28,29,36,37,46,48,43,49,52,53,57], {"12_10_12":0,"26_6_37":0,"26_6_7":0,"26_17_26":0,"34_6_37":0,"34_6_7":0,"34_17_26":0,"42_6_23":0,"50_10_3":0,"56_8_3":0}, ["var http = require('http'),","    util = require('util'),","    async = require('async');","","var baseUrl = \"http://dev.markitondemand.com/Api/v2\";","","var _getRequest = function _getRequest(url, next) {","  http.get(url, function(response) {","    response.on('data', function(data) {","      data = JSON.parse(data);","","      if (data.Message) {","        return next(data.Message);","      }","      else {","        return next(null, data);","      }","    });","  }).on('error', function(e) {","    return next(e.message);","  });","};","","","var lookup = function lookup(symbol, next) {","  if (!symbol || typeof symbol !== 'string') return next(\"Invalid query\");","","  var url = baseUrl + \"/Lookup/json?input=\" + symbol;","  _getRequest(url, next);","}","","","var getQuote = function getQuote(symbol, next) {","  if (!symbol || typeof symbol !== 'string') return next(\"Invalid symbol\");","","  var url = baseUrl + \"/Quote/json?symbol=\" + symbol;","  _getRequest(url, next);","};","","","var getQuotes = function getQuotes(symbols, next) {","  if (!Array.isArray(symbols)) { ","    return next(\"Invalid type, must provide an array of symbols\"); ","  }","","  var result = [];","","  async.each(symbols, function(symbol, done) {","    getQuote(symbol, function(err, data) {","      if (err) return done(err); ","","      result.push(data);","      return done();","    });","  }, function (err) {","    if (err) return next(err);","    return next(null, result);","  });","};","","","module.exports.lookup    = lookup;","module.exports.getStock  = lookup;","module.exports.getQuote  = getQuote;","module.exports.getQuotes = getQuotes;",""]);
_$jscmd("lib/markitondemand.js", "line", 1);

var http = require("http"), util = require("util"), async = require("async");

_$jscmd("lib/markitondemand.js", "line", 5);

var baseUrl = "http://dev.markitondemand.com/Api/v2";

_$jscmd("lib/markitondemand.js", "line", 7);

var _getRequest = function _getRequest(url, next) {
    _$jscmd("lib/markitondemand.js", "line", 8);
    http.get(url, function(response) {
        _$jscmd("lib/markitondemand.js", "line", 9);
        response.on("data", function(data) {
            _$jscmd("lib/markitondemand.js", "line", 10);
            data = JSON.parse(data);
            if (_$jscmd("lib/markitondemand.js", "cond", "12_10_12", data.Message)) {
                _$jscmd("lib/markitondemand.js", "line", 13);
                return next(data.Message);
            } else {
                _$jscmd("lib/markitondemand.js", "line", 16);
                return next(null, data);
            }
        });
    }).on("error", function(e) {
        _$jscmd("lib/markitondemand.js", "line", 20);
        return next(e.message);
    });
};

_$jscmd("lib/markitondemand.js", "line", 25);

var lookup = function lookup(symbol, next) {
    if (_$jscmd("lib/markitondemand.js", "cond", "26_6_37", _$jscmd("lib/markitondemand.js", "cond", "26_6_7", !symbol) || _$jscmd("lib/markitondemand.js", "cond", "26_17_26", typeof symbol !== "string"))) return next("Invalid query");
    _$jscmd("lib/markitondemand.js", "line", 28);
    var url = baseUrl + "/Lookup/json?input=" + symbol;
    _$jscmd("lib/markitondemand.js", "line", 29);
    _getRequest(url, next);
};

_$jscmd("lib/markitondemand.js", "line", 33);

var getQuote = function getQuote(symbol, next) {
    if (_$jscmd("lib/markitondemand.js", "cond", "34_6_37", _$jscmd("lib/markitondemand.js", "cond", "34_6_7", !symbol) || _$jscmd("lib/markitondemand.js", "cond", "34_17_26", typeof symbol !== "string"))) return next("Invalid symbol");
    _$jscmd("lib/markitondemand.js", "line", 36);
    var url = baseUrl + "/Quote/json?symbol=" + symbol;
    _$jscmd("lib/markitondemand.js", "line", 37);
    _getRequest(url, next);
};

_$jscmd("lib/markitondemand.js", "line", 41);

var getQuotes = function getQuotes(symbols, next) {
    if (_$jscmd("lib/markitondemand.js", "cond", "42_6_23", !Array.isArray(symbols))) {
        _$jscmd("lib/markitondemand.js", "line", 43);
        return next("Invalid type, must provide an array of symbols");
    }
    _$jscmd("lib/markitondemand.js", "line", 46);
    var result = [];
    _$jscmd("lib/markitondemand.js", "line", 48);
    async.each(symbols, function(symbol, done) {
        _$jscmd("lib/markitondemand.js", "line", 49);
        getQuote(symbol, function(err, data) {
            if (_$jscmd("lib/markitondemand.js", "cond", "50_10_3", err)) return done(err);
            _$jscmd("lib/markitondemand.js", "line", 52);
            result.push(data);
            _$jscmd("lib/markitondemand.js", "line", 53);
            return done();
        });
    }, function(err) {
        if (_$jscmd("lib/markitondemand.js", "cond", "56_8_3", err)) return next(err);
        _$jscmd("lib/markitondemand.js", "line", 57);
        return next(null, result);
    });
};

_$jscmd("lib/markitondemand.js", "line", 62);

module.exports.lookup = lookup;

_$jscmd("lib/markitondemand.js", "line", 63);

module.exports.getStock = lookup;

_$jscmd("lib/markitondemand.js", "line", 64);

module.exports.getQuote = getQuote;

_$jscmd("lib/markitondemand.js", "line", 65);

module.exports.getQuotes = getQuotes;