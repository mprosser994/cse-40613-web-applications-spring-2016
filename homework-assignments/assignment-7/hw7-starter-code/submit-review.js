"use strict";

$function({
    AWS.config.credentials = new AWS.Credentials(/*"Your Access Key", "Your Secret Access Key"*/);
    AWS.config.region = "us-east-1";
    var bucket = new AWS.S3({params: {"Bucket": "the name of your S3 bucket"}});

    // Add your submit logic here
    // Get the name and the review body
    // Get the file object with the line
    //      var file = $("your file input element id").prop("files")[0]
    // Upload parameters should be {Key: file.name, ContentType: file.type, Body: file, ACL: "public-read"}
    // Inside of the bucket.upload callback, push the review to Firebase
});
