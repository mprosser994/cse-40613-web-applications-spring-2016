/**
 * Created by jesus on 2/11/16.
 */
"use strict";
var assert = require('assert');
function polygon() {
    // use variable number and format of arguments now
    var args = arguments;
    function pair(p) {
        // examine pair p and return {x:p[0], y:p[1]}
        if (Array.isArray(p)) {
            return {x:p[0], y:p[1]};
        }
        else if (p.hasOwnProperty('x') && p.hasOwnProperty('y')) {
            return p;
        }
        else {
            throw new Error("Format of points should be [x y] or {x:n,y:n}");
        }
    }
    function convert() {
        console.log("arguments:",args);
        var result = [];
        if (args.length===1) { // one array: [arg1, arg2, arg3...]
            args = args[0];
        }
        if (args.length>1){ //
            for (var e of args) {
                result.push(pair(e));
            }
            return result;
        } else {
            throw new Error("Incorrect arguments sent to polygon()");
        }
    }
    var vtx = convert();
    return {
        vertices: vtx,
        order: function () {
            return this.vertices.length;
        },
        perimeter: function () {
            var result = 0, order = this.order(),
                i, a, b, xd, yd;
            for (i = 0; i < this.order(); i++) {
                a = this.vertices[i % order];
                b = this.vertices[(i + 1) % order];
                xd = a.x - b.x;
                yd = a.y - b.y;
                result += Math.sqrt(xd * xd + yd * yd);
            }
            return result;
        }
    };
}

var p=polygon({x:0,y:0},[1,0],[1,1],[0,1]);
console.log(p);
var p2=polygon([{x:0,y:0},[1,0],[1,1],[0,1]]);
console.log(p2);

function square() {
    var args = arguments;
    assert((args.length === 1 && args[0].length===4) || (args.length===4), "You have to pass 4 points to square");
    var that = polygon(args);
    that.area = function() {return 0;}
    return that;
}

var s=square([0,0],[1,0],[1,1],[0,1]);
console.log(s);