/**
 * Created by jesus on 2/17/16.
 */
"use strict";
// allow user to add a new list item
$(function() {
    // add newItemButton after </ul>
    var $lis = $('ul');
    var newItemButton = $('<div id="newItemButton">' +
        '<button id="showForm">NEW ITEM</button></div>');
    $lis.after(newItemButton);
    // add newItemForm after newItemButton
    var $newItemButton = $('#newItemButton');
    var newItemForm = $('<form id="newItemForm">' +
        '<input type="text" id="itemDesc" placeholder="Add restaurant name" />' +
        '<input type="submit" id="addButton" value="ADD" />' +
        '</form>');
    $newItemButton.after(newItemForm);
    var $newItemForm = $('#newItemForm');
    var $textInput = $('input:text'); // input of type="text"

    // initially, show newItem button and hide newItem form
    $newItemButton.show();
    $newItemForm.hide();

    // on click of newItem button, show newItem form
    $newItemButton.on('click', function() {
        $newItemButton.hide();
        $newItemForm.show();
    });

    // when newItem form is submitted, add new restaurant item to list
    $newItemForm.on('submit', function(e) { // e is event object
        e.preventDefault(); // don't load a new page!
        var newText = $textInput.val();
        var $lastItem = $('li:last');
        if ($lastItem.length>0) {  // if ul is not empty
            $lastItem.removeClass('cool');
        }
        $lis.append($('<li>' + newText + '</li>'));
        $('li:last').addClass('cool');
        $newItemForm.hide();
        $newItemButton.show();
        $textInput.val('');
    });
});
