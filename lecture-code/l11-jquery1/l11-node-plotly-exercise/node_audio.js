"use strict";

// Give this script access to the Baudio module
// npm install baudio --save
var Baudio = require('baudio');

var n = 0;
var b = Baudio(function(t) {
    var x = Math.sin(t * 10 + Math.cos(n / 16));
    n += Math.cos(t);
    return x;
});

b.play();