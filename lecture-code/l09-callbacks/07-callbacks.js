/**
 * Created by jesus on 2/8/16.
 */
"use strict";

function Person(name, dob) {
    this.name = name;
    this.dob = dob;
    this.printName = function() {console.log(this.name);};
    return this;
}
var dan=new Person("Dan",1996);

dan.printName();

console.log(fact(6));

function fact(n) {
    if (n<1) return 1;
    else return n*fact(n-1);
}

/*This code fails:
console.log(myFact(6));
var myFact = function (n) {
    if (n<1) return 1;
    else return n*fact(n-1);
};
*/

function callMeMaybe(msg1,msg2) {
    console.log("We have an important message to deliver...", msg1,"\n"+msg2);
}

var id = setTimeout(callMeMaybe,2000,"The ship will self destruct!","Good luck!");
console.log("setTimeout is asynchronous, life goes on until the event happens");
//clearTimeout(id);

var id2 = setTimeout(function(msg) {
    console.log(msg);
}, 2000, "Calling back!");

var id3 = setInterval(function(msg) {
    console.log(msg);
}, 2000, "Calling back!");

setTimeout(function() {
    console.log("clearing interval event!");
    clearTimeout(id3);}
    ,6000);

var myTimer = function(init,period,msg) {
    var count = init;
    var result = setInterval(function () {
        count++;
        console.log(msg,count);
    }, period);
    return result;
};

var id4 = myTimer(0,100,"Count: ");
setTimeout(function(){
    console.log("clearing timer!");
    clearTimeout(id4);
}, 2100);

Person.call(dan,"Danny",Date(1996,2,28));
dan.printName();

console.log(Math.max.apply(null,[-5,-19,-3]));

